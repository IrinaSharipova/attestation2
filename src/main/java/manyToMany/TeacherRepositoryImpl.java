package manyToMany;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

public class TeacherRepositoryImpl implements TeacherRepository {

    private final Session session;

    public TeacherRepositoryImpl(Session session) {
        this.session = session;
    }


    @Override
    public List<Teacher> getAll() {
        Query<Teacher> teacherNativeQuery = session.createQuery("from Teacher", Teacher.class);
        return teacherNativeQuery.getResultList();
    }
}
