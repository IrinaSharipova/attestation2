package manyToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory()) {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            select(session);

            transaction.commit();

            session.close();
        }
    }

    public static void select(Session session){
        TeacherRepository teacherRepository = new TeacherRepositoryImpl(session);
        StudentRepository studentRepository = new StudentRepositoryImpl(session);

        System.out.println("Students:");
        List<Student> students = studentRepository.getAll();
        System.out.println(students);

        System.out.println("Teachers:");
        List<Teacher> teachers = teacherRepository.getAll();
        System.out.println(teachers);
    }

    public static void insert(Session session) {
        Teacher anna = Teacher.builder()
                .name("Anna")
                .build();

        Teacher viktor = Teacher.builder()
                .name("Viktor")
                .build();

        session.save(anna);
        session.save(viktor);

        Student razil = Student.builder()
                .name("Razil")
                .teachers(List.of(anna, viktor))
                .build();

        Student oleg = Student.builder()
                .name("Oleg")
                .teachers(List.of(anna))
                .build();

        session.save(razil);
        session.save(oleg);

    }
}
