package manyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany(mappedBy = "teachers")
    private List<Student> students;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Student student : students) {
            stringBuilder.append("[Student ")
                    .append(student.getId())
                    .append(" ")
                    .append(student.getName())
                    .append("]");
        }
        return "[Teacher " + id + " " + name + " (" + stringBuilder +  ")]";
    }
}
