package intro;

import lombok.*;

import javax.persistence.*;

//POJO - Plain Old Java Object
//1. Класс не должен быть конечным (не должно быть модификатора final)
//2. Поля класса, так же не должны быть финальными (не должно быть модификатора final)
//3. Обязательно должен быть пустой конструктор
//4. Поля должны быть приватными, доступ к ним - через геттеры и сеттеры
//5. Для того, что бы класс подходил под требования, у него должны быть ОБЯЗАТЕЛЬНО две аннотоции - @Entity и @Id
@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "class", nullable = false)
    private String classAnimal;

    @Column(name = "color", nullable = true)
    private String color;
}

//public class Product {
//
//    private (final) long id;
//    private (final) String name;
//    private (final) String description;
//    private (final) List<Comment> commentaries;
//}
